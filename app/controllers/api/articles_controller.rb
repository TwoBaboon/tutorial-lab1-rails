class Api::ArticlesController < ApiController

  def index
    articles = Article.all
    render json: articles
  end

  def show
    article = Article.find(params[:id])
    if article
      render json: article
    else
      render json: article.errors
    end
  end
#----------------------------------------------------
  def new 
    @article = Article.new
    @article.picture = Picture.new

  end

  def create
    article = Article.new(article_params)

    if article.save
      render json: article.to_json
    else
      render json: article.errors
    end
  end
#----------------------------------------------------

  def edit
    @article = Article.find(params[:id])
    if @article.picture.mon_image.attached?
    else
    @article.picture = Picture.new
    end
  end

  def update
    article = Article.find(params[:id])

    if article.update(article_params)
      render json: article
      #redirect_to @article
    else
      render json: article.errors, status: :unpocessable_entity
    end
  end
#----------------------------------------------------  

  def destroy
    article = Article.find(params[:id])

    article.destroy
    render json: article.to_json

  end


  private
  def article_params
    params.require(:article).permit(:title, :body, :status, picture_attributes: [:id, :mon_image])
  end




end
