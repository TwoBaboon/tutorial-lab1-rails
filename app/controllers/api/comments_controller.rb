class Api::CommentsController < ApiController

#before action  :getarticle



    def index
        article = Article.find(params[:article_id])
        if article
            render json: article.comments
        else
            render json: article.errors
        end
      end


    def show
        article = Article.find(params[:article_id])
        comment = article.comments.find(params[:id])
        if comment
            render json: comment
        else
            render json: comment.errors
        end
    end

    def create
        article = Article.find(params[:article_id])
        comment = article.comments.create(comment_params)
        if comment.save
            render json: comment.to_json
          else
            render json: comment.errors
          end
        
    end

    def destroy
        article = Article.find(params[:article_id])
        comment = article.comments.find(params[:id])

          if comment.destroy
            render json: comment.to_json
          else
            render json: comment.errors
          end
    end



    def update
        article = Article.find(params[:article_id])
        comment = article.comments.find(params[:id])
        if comment.update(comment_params)
          render json: comment
        else
          render json: comment.errors #, status: :unpocessable_entity
        end
    end



private
    def comment_params
        params.require(:comment).permit(:commenter, :body, :status)
    end

   # def getarticle
    #  Article.find(params[:article_id])
   # end

end

