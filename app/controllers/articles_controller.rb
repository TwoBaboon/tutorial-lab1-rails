class ArticlesController < ApplicationController

  before_action :authenticate_user!, except: [:index, :show]


  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end
#----------------------------------------------------
  def new 
    @article = Article.new
    @article.picture = Picture.new

  end

  def create
    @article = Article.new(article_params)

    if @article.save
      redirect_to @article
    else
      @article.picture = Picture.new
      render :new
    end
  end
#----------------------------------------------------

  def edit
    @article = Article.find(params[:id])
    if @article.picture.mon_image.attached?
    else
    @article.picture = Picture.new
    end
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render :edit
    end
  end
#----------------------------------------------------  

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    redirect_to root_path
  end


  private
  def article_params
    params.require(:article).permit(:title, :body, :status, picture_attributes: [:id, :mon_image])
  end




end
