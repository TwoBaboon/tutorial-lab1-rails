class Picture < ApplicationRecord
  belongs_to :imageable, polymorphic: true, optional: true
  has_one_attached :mon_image
end
