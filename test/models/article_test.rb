require "test_helper"

class ArticleTest < ActiveSupport::TestCase
  #rails test test/models/article_test.rb:5   (chiffre = ligne du début du test)





  #il faut enlever les restrictions du visible, du body length et picture

  #En faisant un .new et ensuite un .save, il n'est pas normal que ce soit possible de créer un article sans titre.
  #alors dans le test je fais:

      #article = Article.new (crée un item vide)
      #assert_not article.save  
          #(je save le nouvel item vide, mais le assert_not s'attend à recevoir un false [un fail de création à cause du titre vide])
  
  #en recevant le false, logiquement, le test doit retourner un vrai, car nous nous attendions à recevoir un false.
  #je veux recevoir un false: assert_not
  #je crée un item sans titre, si j'ai créé une validation sur la présence d'un titre, alors il est supposé me retourner false (tester avec rails c au pire)
  #si Assert_not (false) == le_resultat_du_système_lors_d'une_création_de_titre_vide (false)
  #alors le test retourne un pass (un pass à cause que le resultat est ce quoi on s'attendait à recevoir)   
  
#   test "should not save article without title" do  
#     article = Article.new
#     assert_not article.save
#   end


#   #--------------------------------------------------------------------------------------------------

  
test "article should not be created without data" do
  article = Article.new(); 
  assert_not article.save
end

test "article should be created with data" do
  article = Article.new(title:"mon titre", body: "mon body de caca", status: "public")  # remove   title:"mon titre", body: "mon body de caca", status: "public"
  assert article.save, "Saved the article without data"
end

test "article should not be created with missing title" do
  article = Article.new(body: "mon body de caca", status: "public")
  assert_not article.save
end


test "article should not be created with missing body" do
  article = Article.new(title:"mon titre", status: "public")
  assert_not article.save
end


test "article should not be created with missing status" do
  article = Article.new(title:"mon titre", body: "mon body de caca")
  assert_not article.save
end


test "article should not be created with body  with less than 10 chars" do
  article = Article.new(title:"mon titre", body: "mon", status: "public")
  assert_not article.save  
end

test "article with wrong status should not be valid" do
  article = Article.new(title:"mon titre", body: "mon body de caca", status: "tablette")
  assert_not article.save
end


test "article with ok status should be valid" do
  article = Article.new(title:"mon titre", body: "mon body de caca", status: "public")
  assert article.save
end

end
