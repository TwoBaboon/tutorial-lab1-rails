require "test_helper"

class CommentTest < ActiveSupport::TestCase

  test "comment should not be created without data" do
    comment = Comment.new(); 
    assert_not comment.save
  end

  test "comment should be created with data" do
    comment = Comment.new(commenter: "testing name", body: "testing body", status: "public",article: Article.first) #remove commenter: "testing name", body: "testing body", status: "public",article: Article.first
    assert comment.save, "Saved the comment without data"
  end

  test "comment should not be created with missing commenter" do
    comment = Comment.new(body: "testing body", status: "public",article: Article.first)
    assert_not comment.save
  end

  test "comment should not be created with missing body" do
    comment = Comment.new(commenter: "testing name",status: "public",article: Article.first)
    assert_not comment.save
  end


  test "comment should not be created with missing status" do
    comment = Comment.new(commenter: "testing name", body: "testing body",article: Article.first)
    assert_not comment.save
  end

  test "comment should not be created with missing article" do
    comment = Comment.new(commenter: "testing name", body: "testing body", status: "public")
    assert_not comment.save
  end

  test "comment should not be created with body with less than 10 chars" do
    comment = Comment.new(commenter: "testing name", body: "1232", status: "public",article: Article.first)
    assert_not comment.save
    
  end

  test "comment with wrong status should not be valid" do
    comment = Comment.new(commenter: "testing name", body: "testing body", status: "salut",article: Article.first)
    assert_not comment.save
  end

  test "comment with ok status should  be valid" do
    comment = Comment.new(commenter: "testing name", body: "testing body", status: "public",article: Article.first)
    assert comment.save
  end


  test "comment should be deleted" do
    comment = Comment.first
    assert comment.destroy
  end


end
