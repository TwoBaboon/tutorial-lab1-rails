require "test_helper"

class Api::ArticlesControllerTest < ActionDispatch::IntegrationTest

include Devise::Test::IntegrationHelpers

setup do
    @user = users(:one)
    sign_in @user
end
  


#show#show#show#show#show#show#show#show#show#show#show#show#show
test "should receive status 200 when requesting all articles" do
    get "/api/articles/"
    assert_response(200)
end

test "should receive status 200 when accessing existing page" do
    get "/api/articles/1"
    assert_response(200)
end

test "should receive status error when accessing a non existing page" do
    assert_raises(ActiveRecord::RecordNotFound) do
        get "/api/articles/8888"
    end
end
#show#show#show#show#show#show#show#show#show#show#show#show#show



#create#create#create#create#create#create#create#create#create#create#create#create#create#create#create#create#create
test "inserting a valid article should return 200" do
    assert_difference -> { Article.count } do
        post api_articles_path, params: { article:{ title: "mon titre", body: "mon corps oui oui", status: "public"}}
    end
    assert_response(200)
end


test "inserting an invalid article should return 200 but keep the same amount of articles (it returns the error in json)" do
    assert_difference -> { Article.count }, 0 do
        post api_articles_path, params: { article:{ title: "mon titre", body: "mon corps oui oui"}}
    end
    assert_response(200)
end
#create#create#create#create#create#create#create#create#create#create#create#create#create#create#create#create#create


#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy
test "deleting a valid article should return 200 (sends back the json item deleted)" do
    assert_difference -> { Article.count }, -1  do
        delete "/api/articles/1"
    end
    assert_response(200)
end


test "deleting an invalid article should return 404" do
    assert_raises(ActiveRecord::RecordNotFound) do
        delete "/api/articles/45"
    end
end
#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy

#update#update#update#update#update#update#update#update#update#update#update#update#update#update#update#update#update
test "editing a valid article should return 200 (sends back the json item deleted)" do
    assert_difference -> { Article.count }, 0  do
        put "/api/articles/1", params: { article:{title: "mon titre", body: "mon corps oui oui", status: "public"}} # remove title: "mont titre"
    end
    assert_equal(Article.first.title,"mon titre", "something went wrong: The update did not affect the database")
    assert_response(200)
end

test "editing an invalid article should return 404" do
    assert_raises(ActiveRecord::RecordNotFound) do
        put "/api/articles/45"
    end
end
#update#update#update#update#update#update#update#update#update#update#update#update#update#update#update#update#update


end
