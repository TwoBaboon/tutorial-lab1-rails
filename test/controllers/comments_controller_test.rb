require "test_helper"

class CommentsControllerTest < ActionDispatch::IntegrationTest



  #Functional Tests for Your Controllers
  # https://guides.rubyonrails.org/testing.html#functional-tests-for-your-controllers
  #----------------------------------------------
  # was the web request successful?
  # was the user redirected to the right page?
  # was the user successfully authenticated?
  # was the appropriate message displayed to the user in the view?
  # was the correct information displayed in the response?



  
#show#show#show#show#show#show#show#show#show#show#show#show#show

test "should receive status 200 when requesting all comments of an article" do
  get "/api/articles/1/comments"
  assert_response(200)
end

test "should receive status 200 when accessing a specific comment of an article" do
  get "/api/articles/1/comments/1"
  assert_response(200)
end

test "should receive status error when accessing a specific comment of an article that does not exist" do
  assert_raises(ActiveRecord::RecordNotFound) do
      get "/api/articles/1/comments/8888"
  end
end
#show#show#show#show#show#show#show#show#show#show#show#show#show



# #create#create#create#create#create#create#create#create#create#create#create#create#create#create#create#create#create
test "inserting a valid comment to a valid article should return 200" do
  post "/api/articles/1/comments", params: { comment: { commenter: "Luke", body: "congra23423422ts", status: "public" } }
  assert_equal(Comment.last, Comment.new(response.parsed_body))
  assert_response(200)
end

test "inserting a valid comment to an invalid article should return error" do
  assert_raises(ActiveRecord::RecordNotFound) do
    post "/api/articles/6566/comments", params: { comment: { commenter: "Luke", body: "congra23423422ts", status: "public" } }
  end
end


test "inserting an invalid comment to a valid article should return 200 (with an error message in json to the user)" do
  assert_difference -> { Comment.count }, 0 do
    post "/api/articles/1/comments", params: { comment: { commenter: "Luke", body: "congra23423422ts"} }
  end
  assert_response(200)
  assert_equal({"status"=>["is not included in the list"]}, response.parsed_body)
end
# #create#create#create#create#create#create#create#create#create#create#create#create#create#create#create#create#create


# #destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy
test "deleting a valid comment should return 200" do
  assert_difference -> { Comment.count }, -1  do
      delete "/api/articles/1/comments/1"
  end
  assert_response(200)
end

test "deleting an invalid comment should return 404 error" do
  assert_raises(ActiveRecord::RecordNotFound) do
      delete "/api/articles/1/comments/3"
  end
end
# #destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy#destroy

# #update#update#update#update#update#update#update#update#update#update#update#update#update#update#update#update#update
test "editing a valid comment should return 200" do
  assert_difference -> { Comment.count }, 0  do
      put "/api/articles/1/comments/1", params: { comment:{commenter: "mon titre22", body: "mon corps oui oui22", status: "public", article_id: 1}} #remove  commenter: "mon titre22",
  end
  assert_equal(Comment.first.commenter,"mon titre22", "something went wrong: The update did not affect the database")
  assert_response(200)
end

test "editing an invalid comment with a valid article should return 404" do
  assert_raises(ActiveRecord::RecordNotFound) do
      put "/api/articles/1/comments/1123123"
  end
end
# #update#update#update#update#update#update#update#update#update#update#update#update#update#update#update#update#update



end
