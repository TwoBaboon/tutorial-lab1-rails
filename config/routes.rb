Rails.application.routes.draw do
  devise_for :users
  root "articles#index"

  namespace :api, defaults: { format: 'json' } do
    resources :articles do 
      resources :comments
    end
  end


    resources :articles do 
      resources :comments
  end
end
